package br.com.zambe.exception;

public class InputInvalidoException extends RuntimeException {

  public InputInvalidoException(String message) {
    super(message);
  }

}
