package br.com.zambe.exception;

public class EscritaArquivoException extends RuntimeException {

  public EscritaArquivoException(String message) {
    super(message);
  }

}
