package br.com.zambe.validator;

import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

@Component
public class ContaDTOValidator {

  public boolean validate(CSVRecord conta) {
    return conta.size() == 4;
  }
}
