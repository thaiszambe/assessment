package br.com.zambe.gateway;

import br.com.zambe.exception.InputInvalidoException;
import br.com.zambe.usecase.AtualizaContasReceitaUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class InputArquivoMemoriaGateway implements InputArquivoGateway {

  private final AtualizaContasReceitaUseCase atualizaContasReceitaUseCase;

  public void parse(String[] args) {
    String path = Arrays.stream(args).findFirst().orElseThrow(() -> new InputInvalidoException("Informe um arquivo a ser lido"));
    atualizaContasReceitaUseCase.executar(path);
  }
}
