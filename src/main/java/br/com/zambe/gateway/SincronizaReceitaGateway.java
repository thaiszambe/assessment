package br.com.zambe.gateway;

import br.com.zambe.domain.ContaDTO;

public interface SincronizaReceitaGateway {

  boolean enviar(ContaDTO contaDTO);

}
