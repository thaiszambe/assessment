package br.com.zambe.gateway;

import br.com.zambe.domain.ContaDTO;
import br.com.zambe.service.external.ReceitaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SincronizarReceitaFakeGateway implements SincronizaReceitaGateway {

  private final ReceitaService receitaService = new ReceitaService();

  @Override
  public boolean enviar(ContaDTO contaDTO) {
    try {
      return receitaService.atualizarConta(
        StringUtils.getDigits(contaDTO.getAgencia()),
        StringUtils.getDigits(contaDTO.getConta()),
        NumberUtils.createDouble(contaDTO.getSaldo().replaceFirst(",", ".")),
        contaDTO.getStatus());
    } catch (Exception e) {
      log.debug("Não foi possível atualizar a conta, ignorando...", e);
      return false;
    }
  }

}
