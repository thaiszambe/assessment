package br.com.zambe.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContaDTO {

  String agencia;
  String conta;
  String saldo;
  String status;
  Boolean sincronizado;

}
