package br.com.zambe.service;

import br.com.zambe.domain.ContaDTO;
import br.com.zambe.exception.EscritaArquivoException;
import br.com.zambe.gateway.SincronizarReceitaFakeGateway;
import br.com.zambe.mapper.CSVRecordToContaMapper;
import br.com.zambe.validator.ContaDTOValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class AtualizaArquivoService {

  private final ContaDTOValidator contaDTOValidator;
  private final CSVRecordToContaMapper csvRecordToContaMapper;
  private final SincronizarReceitaFakeGateway sincronizarReceitaFakeGateway;

  @Value("${concurrent.entries.max}")
  private int maxConcurrent;

  @Value("${csv.file.delimiter}")
  private char fileDelimiter;

  public static final String MOMENT_NOW = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddhhmmss"));
  public static final String RESULT_FILENAME = MOMENT_NOW + "_exit.csv";
  private static final String[] HEADERS = {"agencia", "conta", "saldo", "status", "sincronizado"};

  public void executar(File entrada) {
    log.info("Iniciando leitura e sincronização de arquivo. Destino -> {}", RESULT_FILENAME);
    CSVFormat fmt = CSVFormat.EXCEL.withDelimiter(fileDelimiter).withFirstRecordAsHeader();
    try (CSVParser parser = CSVParser.parse(entrada, StandardCharsets.UTF_8, fmt)) {

      Iterator<CSVRecord> iterator = parser.iterator();
      List<ContaDTO> contaDTOList = new ArrayList<>();

      while (iterator.hasNext()) {
        CSVRecord data = iterator.next();
        if (contaDTOValidator.validate(data)) {
          contaDTOList.add(csvRecordToContaMapper.map(data));
          if (contaDTOList.size() == maxConcurrent || !iterator.hasNext()) {
            List<ContaDTO> contasAtualizadas = sincronizarContas(contaDTOList);
            escreverContas(contasAtualizadas, Paths.get(entrada.getAbsoluteFile().getParent(), RESULT_FILENAME));
            contaDTOList.clear();
          }
        }
      }
      log.info("Escrita de arquivo finalizada");
    } catch (Exception e) {
      log.debug("Ocorreu um erro inesperado na escrita do arquivo", e);
      throw new EscritaArquivoException("Não foi possível escrever o arquivo com os dados atualizados");
    }
  }

  private List<ContaDTO> sincronizarContas(List<ContaDTO> contaDTOList) {
    List<CompletableFuture<Boolean>> futures = contaDTOList.stream()
      .map(c -> CompletableFuture.supplyAsync(() -> sincronizarReceitaFakeGateway.enviar(c)))
      .collect(Collectors.toList());
    CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

    return IntStream
      .range(0, contaDTOList.size())
      .mapToObj(i -> {
        contaDTOList.get(i).setSincronizado(futures.get(i).join());
        return contaDTOList.get(i);
      })
      .collect(Collectors.toList());
  }

  private void escreverContas(List<ContaDTO> contasAtualizadas, Path path) throws IOException {
    boolean isFile = Files.exists(path);
    BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
    try (writer; CSVPrinter printer = new CSVPrinter(writer, CSVFormat.EXCEL
      .withDelimiter(fileDelimiter)
      .withSkipHeaderRecord(isFile)
      .withHeader(HEADERS))) {
      for (ContaDTO c : contasAtualizadas) {
        printer.printRecord(c.getAgencia(), c.getConta(), c.getSaldo(), c.getStatus(), c.getSincronizado());
      }
    } catch (Exception e) {
      log.debug("Exceção lançada ao escrever em arquivo", e);
    }
  }

}
