package br.com.zambe.service;

import br.com.zambe.exception.LeituraArquivoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;

@Slf4j
@Service
public class CarregaArquivoService {

  public File executar(String path) {
    try {
      File arquivo = new File(path);
      if (!arquivo.exists()) {
        throw new LeituraArquivoException("Arquivo não encontrado");
      }
      if (!arquivo.canRead()) {
        throw new LeituraArquivoException("A aplicação não possui permissões para ler o arquivo especificado");
      }
      return arquivo;
    } catch (LeituraArquivoException e) {
      throw e;
    } catch (Exception e) {
      log.debug("Ocorreu um erro inesperado na leitura do arquivo", e);
      throw new LeituraArquivoException("Não foi possível ler o arquivo informado");
    }
  }

}
