package br.com.zambe.usecase;

import br.com.zambe.exception.EscritaArquivoException;
import br.com.zambe.exception.LeituraArquivoException;
import br.com.zambe.service.AtualizaArquivoService;
import br.com.zambe.service.CarregaArquivoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;

@Slf4j
@Component
@RequiredArgsConstructor
public class AtualizaContasReceitaUseCase {

  private final CarregaArquivoService carregaArquivoService;
  private final AtualizaArquivoService atualizaArquivoService;

  public void executar(String path) {
    try {
      File entrada = carregaArquivoService.executar(path);
      atualizaArquivoService.executar(entrada);
    } catch (LeituraArquivoException e) {
      log.error("Ocorreu um erro na leitura do arquivo", e);
    } catch (EscritaArquivoException e) {
      log.error("Ocorreu um erro na escrita do arquivo", e);
    }
  }

}
