package br.com.zambe.mapper;

import br.com.zambe.domain.ContaDTO;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

@Component
public class CSVRecordToContaMapper {

  public ContaDTO map(CSVRecord origem) {
    return ContaDTO.builder()
      .agencia(origem.get(0))
      .conta(origem.get(1))
      .saldo(origem.get(2))
      .status(origem.get(3))
      .build();
  }

}
